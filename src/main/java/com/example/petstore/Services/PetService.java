package com.example.petstore.Services;

import com.example.petstore.Models.BaseModels.BaseEntity;
import com.example.petstore.Models.Pet;
import com.example.petstore.Repositories.IPetRepository;
import com.example.petstore.Services.Interfaces.IPetService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PetService implements IPetService {
    private final IPetRepository petRepository;
    public PetService(IPetRepository repo){
        petRepository=repo;
    }
    @Override
    public void Add(BaseEntity entity) {
       petRepository.save((Pet) entity);
    }
    @Override
    public List<Pet> GetAll(){
        return petRepository.findAll();
    }
}
