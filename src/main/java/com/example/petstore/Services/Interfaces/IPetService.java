package com.example.petstore.Services.Interfaces;

import com.example.petstore.Models.BaseModels.BaseEntity;
import com.example.petstore.Models.Pet;

import java.util.List;

public interface IPetService {
    public void Add(BaseEntity entity);
    public List<Pet> GetAll();
}
