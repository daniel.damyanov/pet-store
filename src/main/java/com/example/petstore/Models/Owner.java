package com.example.petstore.Models;

import com.example.petstore.Models.BaseModels.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Entity
@Table(name="owners")
public class Owner extends BaseEntity {
   @Getter
   @Setter
   public String name;
   @OneToMany(fetch = FetchType.EAGER)
   private List<Pet> pets;

}
