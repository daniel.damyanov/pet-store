package com.example.petstore.Controllers;

import com.example.petstore.Models.BaseModels.BaseEntity;
import com.example.petstore.Models.Pet;
import com.example.petstore.Services.PetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/store")
@CrossOrigin("http://localhost:8080")
public class StoreController {
    @Autowired
private PetService petService;
public StoreController(PetService service){
    petService=service;
}
@GetMapping
public ResponseEntity<List<Pet>> GetAll(){
    return  ResponseEntity.ok(petService.GetAll());
}
}
